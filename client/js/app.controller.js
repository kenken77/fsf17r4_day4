/**
 * Client side app.controller.js
 * AngularJS
 */
(function () {
    "use strict";
    angular.module("PopQuizApp").controller("PopQuizCtrl", PopQuizCtrl);

   PopQuizCtrl.$inject = ["$http", "$scope"];
   function PopQuizCtrl($http, $scope){
       var self = this;
       self.quiz = {

       };

       self.finalAnswer = {
           choice: "",
           remarks: "",
       }
       
       self.isCorrect = "";

       self.initForm = function(){
            $http.get("/api/popquizes").then((result)=>{
                console.log(result);
                self.quiz = result.data;
            }).catch((e)=>{
                console.log(e);
            });
       }
       self.initForm();

       self.submitQuiz = ()=>{
          console.log(self.finalAnswer.choice);
          console.log(self.finalAnswer.remarks);
          $http.post("/api/submitQuizes", self.finalAnswer)
            .then((result)=> {
                self.isCorrect = result.data;
            }).catch((e)=>{
                console.log(e);
            });
       }
   }
   
})();
