/**
 * Client side app.module.js
 * AngularJS
 */
"use strict";
 (function () {
    angular.module("PopQuizApp", []);
})();
