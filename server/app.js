/**
 * App.js Server side
 * Express middleware
 */
var express = require("express");
var app = express();
var bodyParser = require("body-parser");

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));

app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));

var popQuiz = require("./quizes.json");
console.log(popQuiz);

app.get("/api/popquizes", (req, res)=>{
    console.log("get popquizes");
    res.status(200).json(popQuiz);
});

app.post("/api/submitQuizes", (req, res)=>{
    console.log("submit popquizes");
    console.log(req.body);
    // convert body choice to integer.
    var choiceInt = parseInt(req.body.choice);
    var correctAnswer = parseInt(popQuiz.correctAnswer);
    // check whether the answer is correct
    console.log(choiceInt);
    console.log(correctAnswer);
    console.log(choiceInt == correctAnswer);
    if(choiceInt == correctAnswer){
        console.log("Its correct !");
        popQuiz.correctMessage = "Its correct !";
    }else{
        console.log("Its incorrect !");
        popQuiz.correctMessage = "Its incorrect !";
    }
    res.status(200).json(popQuiz);
});

app.use((req,res)=>{
    var y = 6;
    try{
        res.send(`<h1> Oopps wrong please ${y}</h1>`);
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    } 
});

app.listen(NODE_PORT, ()=>{
    console.log(`Web App started at ${NODE_PORT}`);
});